
Array.prototype.shuffle = function (){
	var i = this.length, j, temp;
	if ( i == 0 ) return;
	while ( --i ) {
		j = Math.floor( Math.random() * ( i + 1 ) );
		temp = this[i];
		this[i] = this[j];
		this[j] = temp;
	}
};

function MemoryGame(el,cards,tries)
{
	this._$el = $('#' + el);
	
	this._cardsMaxLength 	= this.setCards(cards);
	this._tries 			= this.setTries(tries);

	this._wrongs 	= 0;
	this._pairs 	= 0;
	
	this._$prevCard = null;
	this._cards 	= [];
	
	this._init();
}

MemoryGame.prototype = {

	_init:function()
	{
		this._reset();
		this._render();
		this._initEvents();
	},
	
	//Public Methods
	
	reload:function()
	{
		this._$el.find('.card-container').off("click", "figure");
		this._$el.find('.card-gameoptions').off( "click", "a");
		
		var self = this;
		this._$el.fadeOut( "fast", function(){
		
			self._$el.html('');
			self._init();
			self._$el.fadeIn("slow");
		});
	},
	
	setCards:function(amount){
	
		var amount = amount || 20;
		this._cardsMaxLength = (amount %2 == 0) ? amount : 20;
		
		return this._cardsMaxLength;
	},
	
	setTries:function(amount){
	
		var amount = amount || this._cardsMaxLength/2;
		var tries = this._cardsMaxLength/2;
		this._tries = (amount <= tries) ? amount : tries;
		
		return this._tries;
	},
	
	//Private Methods

	_reset:function()
	{
		var cardNum = 0;
		for(var i = 0; i < this._cardsMaxLength;i++)
		{
			cardNum += (cardNum >= this._cardsMaxLength/2) ? -(this._cardsMaxLength/2 - 1) : 1;
			this._cards[i] = ({isFlipped:false,number:cardNum});
		}
		
		this._wrongs 	= 0;
		this._pairs 	= 0;
		
		this._cards.shuffle();
	},
	
	_initEvents:function()
	{
		var self = this;
		
		//Pick Option
		this._$el.find('.card-gameoptions').on( "click", "a", function(e){
			
			var t 			= e.target;
			if(t.getAttribute('data-reload'))
			{
				self.reload();
			}
		});
		
		//Pick Card
		this._$el.find('.card-container').on( "click", "figure", function(e){
			
			var t 			= e.target;
			var $elCard 	= $(t.parentNode);
			var idCard		= $elCard.index()
			
			if(self._cards[idCard].isFlipped || (self._tries - self._wrongs) < 1)
			{
				return;
			}
			
			if(self._$prevCard)
			{
				var idPrevCard = self._$prevCard.index();
				if(idCard == idPrevCard)
				{
					return;
				}
				
				//Flip second card.
				$elCard.toggleClass('card-flipped');
				self._cards[idCard].isFlipped = true;
				
				if(self._cards[idCard].number != self._cards[idPrevCard].number)
				{
					self._wrongs++;
					
					self._cards[idCard].isFlipped 		= false;
					self._cards[idPrevCard].isFlipped 	= false;
					
					var $tmpPrev = self._$prevCard;
					setTimeout(function(){
					
						$tmpPrev.toggleClass('card-flipped');
						$elCard.toggleClass('card-flipped');
						$tmpPrev = null;
						
						if(self._wrongs >=  self._tries)
							self._printGameState(self._getHTML_gameover());
						else
							self._printGameState(self._getHTML_tries());
						
					},1000);
				}
				else
				{
					self._pairs++;
					
					if(self._pairs >= self._cardsMaxLength/2)
					{
						self._printGameState(self._getHTML_gamewon());
					}
				}

				self._$prevCard = null;
				return;
			}
			
			//Flip first card.
			self._cards[idCard].isFlipped = true;
			$elCard.toggleClass('card-flipped');
			self._$prevCard = $elCard;
		});
	},
	
	_printGameState:function(text){
		this._$el.find('.card-gamestate').html(text);
	},
	
	_getHTML_tries:function()
	{
		var left = (this._tries - this._wrongs);
		var txtTries = (left > 1) ? 'tries' : 'try';
	
		return '<strong>' + left + '</strong> out of ' + this._tries + ' ' + txtTries  + ' left.';
	},
	
	_getHTML_gameover:function()
	{
		var pairs = this._pairs;
		var txtPairs = (pairs > 1) ? 'pairs' : 'pair';
		return 'GAME OVER! ' + pairs + ' ' + txtPairs + ' found.';
	},
	
	_getHTML_gamewon:function()
	{
		return 'GAME WON!';
	},
	
	_render:function()
	{
		var strHTML = "";
		
		//HTML
		strHTML += '<div class="card-container">';
		
		for(var i = 0; i < this._cardsMaxLength;i++)
		{
			strHTML += '<div class="card">';
			strHTML += '<figure class="card-front"></figure>';
			strHTML += '<figure class="card-back">' + this._cards[i].number + '</figure>';
			strHTML += '</div>';
		}
	
		strHTML += '</div>';
		strHTML += '<div class="card-gamestate">' + this._getHTML_tries() + '</div>';
		
		strHTML += '<div class="card-gameoptions">'
		strHTML += '<a data-reload="true" href="#restart">Restart</a>';
		strHTML += '</div>';
	
		this._$el.html(strHTML);
	}

};

var memory = new MemoryGame('memory',20);


